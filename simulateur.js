document.getElementById("start").addEventListener("click", start);

var casesbrules = []

const data = {
  "dimension": [12, 12],
  "positions_initiales": [[6, 6], [6, 7]],
  "probabilite_propagation": 0.5
}


function start() {
   
 // to do : probleme cross domain
 // data = getConfig();

  removeTables();
  var casesbrules = [];
  var casesAbruler = [];
  var dimension = data.dimension;
  let i = 0;
  do {
    i = i + 1;
    casesAbruler = getCasesAbrules(casesbrules, calculerNombre(data.probabilite_propagation), dimension);
    createTable(i, dimension, casesAbruler, casesbrules);
    casesbrules = casesbrules.concat(casesAbruler);
  } while (casesAbruler.length != 0);
}

function getCasesAbrules(casesbrules, maxCasesAbrules, dimension) {


  var resultat = [];
  if (casesbrules.length == 0) {
    return data.positions_initiales;
  }

  for (let i = 0; i < casesbrules.length; i++) {
  
    var casesAbrules = [];
    
    var x = casesbrules[i][0], y = casesbrules[i][1];
   
    if (x+1 <= dimension[0] && casesAbrules.length < maxCasesAbrules && !tableExists([x+1, y], casesbrules)) {
      casesAbrules = casesAbrules.concat([[x+1, y]]);
    }

    if (x-1 >= 0 && casesAbrules.length < maxCasesAbrules && !tableExists([x-1, y], casesbrules)) {
      casesAbrules = casesAbrules.concat([[x-1, y]]);
    }

    if (y+1 <= dimension[1] && casesAbrules.length < maxCasesAbrules && !tableExists([x, y+1], casesbrules)) {
      casesAbrules = casesAbrules.concat([[x, y+1]]);
    }

    if (y-1 >= 0 && casesAbrules.length < maxCasesAbrules && !tableExists([x, y-1], casesbrules)) {
      casesAbrules = casesAbrules.concat([[x, y-1]]);
    }
   
    resultat = resultat.concat(casesAbrules);
  }
  return resultat;
}


function createTable(id, dimension, casesAbruler, casesbrules) {
  var table = document.createElement("table" + id);
  table.setAttribute('border', '1');
  var tbody = document.createElement('tbody');
  for (var i = 0; i < dimension[0]; i++) {
    var row = document.createElement('tr');
    for (var j = 0; j < dimension[1]; j++) {
      var cell = getCell([i, j], casesAbruler, casesbrules);
      row.appendChild(cell);
    }
    tbody.appendChild(row);
  }
  table.appendChild(tbody);
  var container = document.getElementById('tables-container');
  
 
  var h3 = document.createElement("h3");
  h3.textContent = "ETAPE : " + id;
  var div = document.createElement('div');
  div.appendChild(h3);
  container.appendChild(div);
   // to do : switch with css style
  var br = document.createElement('br');
  container.appendChild(br);
  container.appendChild(table);

}

function removeTables() {
  document.getElementById('tables-container').innerHTML = '';
}

function getCell(targetCase, casesAbruler, casesbrules) {
  var cell = document.createElement('td');
  cell.textContent = "(" + targetCase[0] + "," + targetCase[1] + ")";
  cell.style.border = '1px solid black';
  cell.style.padding = '8px';
  cell.style.color = 'white';

  cell.style.textAlign = 'center';
  if (tableExists(targetCase, casesAbruler)) {
    cell.style.backgroundColor = 'red';
  } else if (tableExists(targetCase, casesbrules)) {
    cell.style.backgroundColor = 'gray';
  } else {
    cell.style.backgroundColor = 'green';
  }
  return cell;
}


function tableExists(table, tablesArray) {
  for (var i = 0; i < tablesArray.length; i++) {
    if (arraysEqual(table, tablesArray[i])) {
      return true;
    }
  }
  return false;
}

function arraysEqual(arr1, arr2) {
  if (arr1.length !== arr2.length) return false;
  for (var i = 0; i < arr1.length; i++) {
    if (arr1[i].toString() !== arr2[i].toString()) return false;
  }
  return true;
}

function calculerNombre(percentage) {
  var nombre = Math.round(percentage * 4);
  return nombre;
}

function getConfig() {
  fetch('config.json')
  .then(response => {
    if (!response.ok) {
      throw new Error('Erreur lors de la récupération du fichier');
    }
  })
  .then(data => {
    return data;
  })
  .catch(error => {
    console.error('Erreur lors de la récupération de la configuration:', error);
  });
}

